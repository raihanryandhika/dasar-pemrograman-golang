package main 

import("fmt")
import("math/rand")
import("time")

func main() {
	dataDiri()
	randomSoal()
	
}

func dataDiri(){
	var nama string
	var kelas string

	fmt.Println("Quiz MATEMATIKA")
	fmt.Printf("Masukan nama (jangan menggunakan spasi) : ")
	fmt.Scan(&nama)
	fmt.Printf("Masukan kelas (jangan menggunakan spasi) : ")
	fmt.Scan(&kelas)
	
}

func randomSoal(){

	rand.Seed(time.Now().UTC().UnixNano())
		paketSoal := rand.Intn(2) 
		fmt.Println("Paket soal yang harus kamu kerjakan adalah paket soal nomer : ", paketSoal)
		fmt.Println("\n")
		if paketSoal==0{
			fmt.Println("Paket Soal 0")
			totalScore := 0
			soal0(1,"70 : 2", 35, &totalScore)
			soal0(2,"12 : 3", 4, &totalScore)
			soal0(3,"12 x 2", 24, &totalScore)
			fmt.Println("Score", totalScore)
		}else if paketSoal==1{
			fmt.Println("Paket Soal 1")
			totalScore := 0
			soal1(1,"90 : 2", 45,&totalScore)
			soal1(2,"12 : 6", 2, &totalScore)
			soal1(3,"12 x 4", 48, &totalScore)
			fmt.Println("Score", totalScore)
		}else{
			fmt.Println("Maaf, paket soal belum dibuat")
		}
}		

func soal0(nomor int, soal string, jawaban int, scorePtr *int) {
	var input int
	
	fmt.Printf("%d. %s \n", nomor, soal)
	fmt.Println(jawaban)
	rand.Seed(time.Now().UTC().UnixNano())
		pg1 := rand.Intn(100)
		pg2 := rand.Intn(100) 
	fmt.Println(pg1)
	fmt.Println(pg2, "\n")
	fmt.Scan(&input, "\n")

	if jawaban == input {
		fmt.Println("Benar! \n")
		*scorePtr += 10
	}else{
		fmt.Println("Salah!\n")
	}
}
func soal1(nomor1 int, soal1_1 string, jawaban1 int, scorePtr1 *int)  {
	var input int

	fmt.Printf("%d. %s \n", nomor1, soal1_1)
	rand.Seed(time.Now().UTC().UnixNano())
		pg1_1 := rand.Intn(100)
		pg2_1 := rand.Intn(100)
	fmt.Println(pg1_1) 
	fmt.Println(jawaban1)
	fmt.Println(pg2_1, "\n")
	fmt.Scan(&input, "\n")

	if jawaban1 == input {
		fmt.Println("Benar! \n")
		*scorePtr1 += 10
	}else{
		fmt.Println("Salah!\n")
	}
}
