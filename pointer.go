package main

import(
	"fmt"
)

type Addres struct{
	Kota, Provinsi, Negara		string
}

func ChangeCountry(addres *Addres){
	addres.Negara = "Indonesia"
}

func main(){
	addres1 := Addres{"Bandung", "Jawa Barat", "Indonesia"}
	addres2 := &addres1
	addres3 := &addres1

	addres2.Kota = "Jakarta"

	*addres3 = Addres{"Malang", "Jawa Timur", "Indonesia"}

	fmt.Println(addres1)
	fmt.Println(addres2)
	fmt.Println(addres3)

	var addres4 *Addres = new(Addres)
	addres4.Kota = "New York"
	fmt.Println(addres4)

	alamat := Addres{
		Kota : "Yogyakarta",
		Provinsi : "DIY",
		Negara : "",
	}

	ChangeCountry(&alamat)
	fmt.Println(alamat)

}