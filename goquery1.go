package main

import (
  "fmt"
  "log"
  "net/http"

  "github.com/PuerkitoBio/goquery"
)

func main() {
  // Request the HTML page.
  res, err := http.Get("https://exppar.bisaai.id/status.html")
  if err != nil {
    log.Fatal(err)
  }
  defer res.Body.Close()

  // Load the HTML document
  doc, err := goquery.NewDocumentFromReader(res.Body)
  if err != nil {
    log.Fatal(err)
  }

  // Find the review items
  doc.Find("pre").Each(func(i int, s *goquery.Selection) {
    txt := s.Text()
    fmt.Printf(txt)
  })
}