package main

import(
	"fmt"
)

func Ups(i int) interface{} {
	if i == 1{
		return 200
	} else if i == 2{
		return false
	} else {
		return "Tiga"
	}
}

func main(){
	var angka interface{} = Ups(3)
	
	fmt.Println(angka)
	
}