package main 

import(
	"fmt"
)

//DEFER
func logging(){
	fmt.Println("Selesai memanggil function")
}

func runApp(value int){
	defer logging()
	fmt.Println("Berhasil menjalankan app")
	result := 10 / value
	fmt.Println(result)
}

//PANIC
func endApp(){
	message := recover()
	fmt.Println("Telah terjadi error dengan message :", message)
	fmt.Println("App selesai")
}

func runApplicatiion(error bool){
	defer endApp()
	if error {
		panic("APP ERROR")
	}
	
}

func main(){
	runApp(2)
	runApplicatiion(true)
}