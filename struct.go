package main

import(
	"fmt"
)

type Customer struct{
	Name, Addres	string
	Age				int
}

func (customer Customer) sayHello(name string){ // struct function
	fmt.Println("Hello,", name, "My name is,", customer.Name)
}

func main(){
	var raihan Customer
	raihan.Name = "Raihan Ryandhika"
	raihan.Addres = "Bandung"
	raihan.Age = 16

	fmt.Println(raihan)
	fmt.Println(raihan.Name)
	fmt.Println(raihan.Addres)
	fmt.Println(raihan.Age)

	raihan.sayHello("Bang jago")

	joko := Customer{
		Name: "Joko wakwaw",
		Addres: "Jongol",
		Age: 29,
	}

	fmt.Println(joko)
	fmt.Println(joko.Name)
	fmt.Println(joko.Addres)
	fmt.Println(joko.Age)

}