package main

import(
	"fmt"
)

type HasName interface{
	getName() string
	getNumber() int
}

type Profile struct{
	Name string
	Age int
}

func (hasName Profile) getName() string{
	return hasName.Name 
}

func (hasAge Profile) getNumber() int{
	return hasAge.Age
}

func main(){
	a := Profile{
		Name : "Raihan Ryandhika",
		Age : 16,
	}

	fmt.Println("Hello my name is", a.Name)
	fmt.Printf("I'm %d years old", a.Age)
}