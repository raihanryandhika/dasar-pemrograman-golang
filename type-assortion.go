package main

import(
	"fmt"
)

func random() interface{}{
	return 2
}

func main(){
	var result interface{} = random()
	//var resultString = result.(string) //type assertion
	//fmt.Println(resultString)

	switch value := result.(type){
	case string:
		fmt.Println("Value", value, "is integer")
	case int:
		fmt.Println("Value", value, "is integer")
	default:
		fmt.Println("unknown")
	}
}