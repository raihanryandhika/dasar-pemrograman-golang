package main 

import(
	"fmt"
)

func sayHello(){ // function
	fmt.Println("Hello Go")
}

func sayHelloTo(firstName string, lastName string){ // function parameter
	fmt.Println("Nama saya adalah", firstName, lastName)
}

func getHello(name string) string{ // return value
	if name == ""{
		return "Hello Bang"
	} else {
		return "Hello " + name 
	}
}

func getFullName() (string,string,string){ // return multiple values
	return "Raihan","Halilintar", "Ryandhika"
}

func getCompleteDate() (tanggal int, bulan int, tahun int){ // named return values
	tanggal = 24
	bulan = 4
	tahun = 2004

	return
}

func sumAll(numbers ...int) int{ // function variadic
	total := 0
	for _, number := range numbers{
		total += number
	}
	return total
}

func getGoodBye(name string) string{ // function as value
	return "Good Bye " + name
}

type Filter func(string)string // function as parameter
func sayHelloWithFilter(name1 string, filter Filter){
	nameFiltered := filter(name1)
	fmt.Println("Hello ", nameFiltered)
}

func spamFilter(name1 string)string{ // function as parameter
	if name1 == "Anjing"{
		return "******"
	} else {
		return name1
	}
}

type Blacklist func(name2 string)bool
func registUser(name2 string, blacklist Blacklist){ // anonymous function
	if blacklist(name2) {
		fmt.Println("You are blocked", name2)
	} else {
		fmt.Println("Welcome", name2)
	}
}

func factorialRecursive(valuee int)int{ // recursive function
	if valuee == 1{
		return 1
	} else {
		return valuee * factorialRecursive(valuee-1)
	}
}

func main(){
	sayHello()
	sayHelloTo("Raihan","Ryandhika")
	sayHelloTo("Bang","Jago")

	result := getHello("Raihan")
	fmt.Println(result)
	fmt.Println(getHello(""))

	namaDepan, _ , namaAkhhir := getFullName()// (_) jika ada variable yang tidak di perlukan
	fmt.Println(namaDepan, namaAkhhir)

	a , b , c := getCompleteDate()
	fmt.Println("Tanggal lahir saya adalah", a,b,c)

	total := sumAll(80,85,88,95,96,79)
	slice := []int{80,85,88,95,96,79}
	fmt.Println("Total nilai rapot semester 1 = ",total)
	rataRata := total/len(slice)
	fmt.Println("Rata rata = ", rataRata)

	goodBye := getGoodBye
	bye := goodBye("Raihan")
	fmt.Println(bye)

	sayHelloWithFilter("Anjing", spamFilter)
	sayHelloWithFilter("Asep", spamFilter)

	blacklist := func(name2 string) bool{
		return name2 == "Raihan"
	}
	blacklist2 := func(name2 string) bool{
		return name2 == "Anjing"
	}
	registUser("Raihan", blacklist)
	registUser("Anjing", blacklist2)
	registUser("Desphan", func(name2 string)bool{
		return name2 == "Desphan"
	})
	registUser("Elaina", blacklist)


	recursive := factorialRecursive(10)
	fmt.Println(recursive)
}