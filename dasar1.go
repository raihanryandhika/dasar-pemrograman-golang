package main

import(
	"fmt"
	"strings"
	"math/rand"
	"time"
	"math"
)

func main(){
	var names = []string{"Raihan", "Ryandhika"}
	printMessage("Halo", names)

	rand.Seed(time.Now().Unix())
	var randomValue int
	randomValue = randomWithRange(2,100)
	fmt.Println("Angka keberuntungan anda adalah :", randomValue)

	divideNumber(468,12)
	divideNumber(20,20)
	divideNumber(144,12)

	var diameter float64 = 7
	var luas, keliling = calculate(diameter)
	fmt.Printf("Luas lingkaran \t\t : %.2f \n", luas)
	fmt.Printf("keliling lingkaran \t : %.2f\n", keliling)
}

func printMessage(message string , arr []string){
	var nameString = strings.Join(arr, " ")
	fmt.Println(message, nameString)
}

func randomWithRange(min, max int) int {
	var value = rand.Int() % (max-min + 1) + min
	return value
}

func divideNumber(m,n int){
	if n == 0 {
		fmt.Printf("Invalid divider. %d cannot divided by %d\n", m,n)
		return
	}

	var res = m/n
	fmt.Printf("%d / %d = %d\n", m, n, res)
}
 func calculate(d float64) (luas float64, keliling float64){
 	//hitung luas
 	luas = math.Pi * math.Pow(d / 2, 2)//math.Pow digunakan untuk memangkatkan nilai
 	//hitung keliling
 	keliling = math.Pi * d//math.Pi konstanta yg mempresentasikan Pi

 	//kembalikan 2 nilai
 	return 
 }
