package main 

import(
	"fmt"
)

type Man struct{
	Name, MiddleName string
}

func (man *Man) Married(){
	man.Name = "Mr. " + man.Name + man.MiddleName
}

func main(){
	nm := Man{"Raihan ", "Ryandhika"}
	nm.Married()
	fmt.Println(nm.Name)
}