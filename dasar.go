package main

import "fmt"

func main(){

	//STRING

	firstName, lastName := "Raihan", "Ryandhika"
	middleName := new(int)
	_ = 16 //ini adalah reserved variable
	message := `Saya bersekolah di SMKN 2 Bandung.
Saya sekarang kelas 11 jurusan RPL 
dan saya sekarang sedang PKL di Bisa.Ai` //menggunakan simbol backticks (``)

	fmt.Println("Halo", firstName, *middleName, lastName + "!")
	fmt.Printf("Halo nama saya %s %s! ", firstName, lastName)
	fmt.Println(message)
	fmt.Printf("\n")

	//INTEGER

	positiveNumber, negativeNumber, decimalNumber := 69, -1234567, 3.14

	fmt.Printf("Bilangan positif = %d \n", positiveNumber)
	fmt.Printf("Bilangan negatif = %d \n", negativeNumber)
	fmt.Printf("Bilangan desimal = %f \n", decimalNumber)//%f = 6 angka dibelakang koma
	fmt.Printf("Bilangan desimal = %.2f \n", decimalNumber)//%.2f = 2 angka dibelakang koma

	//OPERATOR

	value := (((2 + 6) % 3) * 4 - 2) / 3
	isEqual := (value == 2)

	fmt.Printf("Nilai %d (%t) \n", value, isEqual)

	left := false
	right := true

	leftAndRight := left && right
	fmt.Printf("left && right \t(%t)\n", leftAndRight)

	leftOrRight := left || right
	fmt.Printf("left || right \t(%t)\n", leftOrRight)

	leftReverse := !left
	fmt.Printf("!left \t\t(%t)\n", leftReverse)//\t adalah 1 tab
	fmt.Printf("\n")

	//SELEKSI KONDISI (IF, ELSEIF, ELSE, SWITCH CASE)

	//VAR SEMENTARA PADA IF ELSE

	point := 8840.0

	if percent := point/100; percent >= 100{
		fmt.Printf("%.1f%s perfect!\n", percent, "%")
	} else if percent >= 70{
		fmt.Printf("%.1f%s good\n", percent, "%")
	}else{
		fmt.Printf("%.if%s not bad\n", percent, "%")
	}

	//NORMAL SWITCH CASE

	point1 := 80
	point2 := 40
	point3 := 0
	point4 := 6

	switch point1{
	case 100:
		fmt.Printf("Hebat!")
	case 80,90:
		fmt.Printf("Boleh juga \n")
	default:
		{
			fmt.Printf("Semangat")
			fmt.Printf("Ayo belajar lagi")
		}
	}

	//SWITCH CASE BERGAYA IF ELSE

	switch {
	case (point2 >= 90 && point2 <= 100):
		fmt.Printf("Sempurna")
	case (point2 >= 50 && point2 < 90):
		fmt.Printf("Lumayan")
	default:
		{
		fmt.Printf("Semangat \n")
		fmt.Printf("Ayo belajar lagi \n")
		}
	}

	//SELEKSI KONDISI BERSARANG

	if point3 > 70 {
		switch point3{
		case 100:
			fmt.Println("Sempurna")
		default:
			fmt.Println("Nice")
		}
	} else {
		if point3 == 60 {
			fmt.Println("Lumayan")
		} else if point3 == 50{
			fmt.Println("Belajar lagi")
		} else {
			fmt.Printf("Belajar lebih keras! ")
			if point3 == 0{
				fmt.Println("Blok!")
			}
		}
	}

	//FALLTHROUGH, memaksa melanjutkan ke case selanjutnya

	switch {
	case point4 == 8:
    	fmt.Println("perfect")
	case (point4 < 8) && (point4 > 3):
    	fmt.Println("Awesome")
    fallthrough
	case point4 < 5:
    	fmt.Println("You need to learn more")
    	fmt.Println("")
	default:
    	{
        fmt.Println("not bad")
        fmt.Println("you need to learn more")
    	}
	}

	//FOR

	a := 0
	b := 0

	for a < 3{
		fmt.Println("Angka", a)
		a++
	}

	fmt.Println("")

	for {
		fmt.Println("Angka", b)


		b++
		if b == 3 {
			break
		}
	}

	fmt.Println("")

	for c:=0; c<=100; c++{
		if c % 2 == 0{
			continue
		}

		if c > 10 {
			break
		}

		fmt.Println("Angka", c)
		//1,3,5,7,9
	}

	fmt.Println("")

	for d := 0; d < 10; d++{
		for e := d; e < 10; e++{
			fmt.Print(e, " ")
		}
		fmt.Println()
	}

	fmt.Println("")

	outerLoop:
	for f := 0; f < 5; f++{
		for g := 0; g < 5; g++{
			if f == 3 {
				break outerLoop
			}
			fmt.Print("matriks [",f,"][",g,"],", "\n")
		}
	}

	fmt.Println("")

	//ARRAY

	var fruits = [4]string{"apple", "grape", "banana", "melon"}
	fmt.Println("Jumlah element \t\t", len(fruits))//len menentukan jumlah buah
	fmt.Println("Isi semua element \t", fruits)
	fmt.Println("Saya suka buah \t\t", fruits[2])

	fmt.Println("")

	var numbers1 = [2][3]int{[3]int{3, 2, 3}, [3]int{3, 4, 5}}
	var numbers2 = [2][3]int{{3, 2, 3}, {3, 4, 5}}

	fmt.Println("numbers1", numbers1)
	fmt.Println("numbers2", numbers2)

	fmt.Println("")

	fruits1 := [4]string{"apel", "mangga", "rambutan", "belimbing"}

	for i := 0; i < len(fruits); i++{
		fmt.Printf("Elemen %d : %s \n", i, fruits1[i])
	}

	fmt.Println("")

	fruits2 := [2]string{"jeruk","buah naga"}

	for a, fruit := range fruits2{
		fmt.Printf("Elemen %d : %s\n", a, fruit)
	}

	fmt.Println("")

	fruits3 := make([]string, 2)
	fruits3[0] = "leci"
	fruits3[1] = "anggur"

	fmt.Println(fruits3)

	fmt.Println("")

	//SLICE, jumlah elemen tidak dituliskan 

	// var fruits = []string{"apple", "grape", "banana", "melon"}

	//	fruits[0:2]		[apple, grape]					semua elemen mulai indeks ke-0, hingga sebelum indeks ke-2
	//	fruits[:]		[apple, grape, banana, melon]	semua elemen
	//	fruits[2:]		[banana, melon]					semua elemen mulai indeks ke-2
	//	fruits[:2]		[apple, grape]					semua elemen hingga sebelum indeks ke-2 
	
	fruits4 := []string{"apel","anggur","pisang","melon"}

	aFruits := fruits4[0:3]
	bFruits := fruits4[1:4]

	fmt.Println(fruits4)
	fmt.Println(aFruits)
	fmt.Println(bFruits)

	//fungsi len(), menghitung jumlah elemen slice
	//fungsi cap(), menghitung kapasitas maksimum slice
	//fungsi append(), menambah elemen pada slice
	//fungsi copy(), digunakan untuk mencopy elemnts slice pada src(parameter ke 2), ke dst (parameter pertama)
	
	fmt.Println(len(fruits4))//4
	fmt.Println(len(aFruits))//3
	fmt.Println(cap(aFruits))//4
	fmt.Println(cap(bFruits))//3

	cFruits := append(fruits4, "pepaya")
	fmt.Println(cFruits)

	fmt.Println("")

	dst := make([]string, 2)
	src := []string{"semangka","rambutan","apel","jeruk"}
	n := copy(dst,src)

	fmt.Println(dst)//semangka rambutan
	fmt.Println(src)//semangka rambutan apel jeruk
	fmt.Println(n)//2

	fmt.Println("")

	//3 index

	dFruits := fruits4[0:2:2]
	fmt.Println(dFruits)
	fmt.Println(len(dFruits))
	fmt.Println(cap(dFruits))
	fmt.Println("")

	//MAP

	var chicken = map[string]int{
		"Januari" : 50,
		"Februari" : 40,
		"Maret" : 34,
		"April" : 67,
	}

	fmt.Println("Januari", chicken["Januari"])
	fmt.Println("Mei", chicken["Mei"])

	delete(chicken, "Januari")

	for key, val := range chicken{
		fmt.Println(key, " \t:", val)
	}

	var value1, isExist = chicken["Mei"]
	if isExist{
		fmt.Println(value1)
	} else {
		fmt.Println("Item is not exist")
	}

	fmt.Println("")

	//KOMBINASI SLICE DAN MAP

	var chickens = []map[string]string{
    {"name": "chicken blue",   "gender": "male"},
    {"name": "chicken red",    "gender": "male"},
    {"name": "chicken yellow", "gender": "female"},
	}

	for _, chicken1 := range chickens {
		fmt.Println("Gender \t: ", chicken1["gender"], "Name \t:", chicken1["name"])
	}
}